package lv.sda;

import lv.sda.model.entity.races.RpCharacter;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private static final Player instance = new Player();
    private RpCharacter character; //current character we are playing with
    private List<RpCharacter> characters = new ArrayList<>(); //database of characters


    private Player(){}

    public static Player getInstance(){
        return instance;
    }

    public RpCharacter getCharacter() {
        return character;
    }

    public void setCharacter(RpCharacter character) {
        this.character = character;
    }

    public List<RpCharacter> getCharacters() {
        return characters;
    }

    public void setCharacters(List<RpCharacter> characters) {
        this.characters = characters;
    }
}
