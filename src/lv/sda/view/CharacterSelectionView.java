package lv.sda.view;

import lv.sda.Player;
import lv.sda.model.entity.races.RpCharacter;
import lv.sda.util.UserInputService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

//view
public class CharacterSelectionView {
    public static RpCharacter fetchSelectedCharacter() {

       int index = Integer.parseInt(
                  UserInputService.getUserInputMultiple("Please select your character", fetchChoices()).toString() //"0" "1" "2"
        );

       return Player.getInstance().getCharacters()
        .get(index);
    }

    private static List<String> fetchChoices() {
        List<String> choices = new ArrayList<>();
        Player.getInstance().getCharacters().forEach(player -> {
            choices.add(player.getNickname());
        });
        return choices;
    }
}
