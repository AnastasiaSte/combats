package lv.sda.view;

import lv.sda.model.entity.races.Race;
import lv.sda.util.UserInputService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RaceSelectionView {
    public static Race askForRace() {
        List<String> choices = new ArrayList<>();
        List<Race> values = Arrays.asList(Race.values());

        values.forEach(race -> {
            choices.add(race.toString()); //HUMAN, ELF, DWARF
        });

        int i = Integer.parseInt(
        UserInputService.getUserInputMultiple("Please, select your race: ", choices).toString()
        );

        return values.get(i);
    }
}
