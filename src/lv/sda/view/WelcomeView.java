package lv.sda.view;

import lv.sda.util.UserInputService;

import java.util.ArrayList;
import java.util.List;

//view
public class WelcomeView {
    public static int displayMainMenu() {
        List<String> choices = new ArrayList<>();
        choices.add("PLAY");
        choices.add("Create new character");
        return  Integer.parseInt(
              UserInputService.getUserInputMultiple("What would you like to do?", choices).toString()
        ); //1 | 2
    }

}
