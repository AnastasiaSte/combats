package lv.sda.view;

import lv.sda.util.UserInputService;

public class NicknameSelectionView {
    public static String askNickname() {
        return UserInputService.getUserInput("Enter character's nickname");
    }
}
