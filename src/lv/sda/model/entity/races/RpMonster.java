package lv.sda.model.entity.races;

import lv.sda.model.entity.classes.Hostile;

public class RpMonster extends RpCharacter implements Hostile {

    @Override
    public boolean hostileTo(Object c) {
       // Arrays.asList(new Class[] {RpCharacter.class});
        return c.getClass().isInstance(RpCharacter.class);
    }
}
