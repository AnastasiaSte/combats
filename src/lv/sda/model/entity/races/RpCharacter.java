package lv.sda.model.entity.races;

import lv.sda.model.entity.classes.SuperHero;

/*
This class represent a character.
 */

public class RpCharacter {

    private String nickname;
    private Race race;
    private Gender gender;
    private SuperHero specialization;
    private Integer hitPoints = 100; //absolute value

    public Integer getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(Integer hitPoints) {
        this.hitPoints = hitPoints;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public SuperHero getSpecialization() {
        return specialization;
    }

    public void setSpecialization(SuperHero specialization) {
        this.specialization = specialization;
    }
}
