package lv.sda.model.entity.races;

public enum Gender {
    MALE, FEMALE
}
