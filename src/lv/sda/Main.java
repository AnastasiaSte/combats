package lv.sda;


import lv.sda.controller.CharacterCreationController;
import lv.sda.controller.PlayController;
import lv.sda.view.WelcomeView;

public class Main {

    public static void main(String[] args) {
       int result = WelcomeView.displayMainMenu();

        switch (result) {
            case 0: PlayController.play();
            case 1:
                CharacterCreationController.createCharacter();
                PlayController.play();
        }

       /* PlayerNewsAgency observable = new PlayerNewsAgency();
        PlayerNews observer = new PlayerNews();

        observable.addObserver(observer);
        observable.setNews("news");
        System.out.println(observer.getNews());*/

    }
}
